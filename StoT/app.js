'use strict';

// simple express server
var fs = require('fs');
var express = require('express');
var path = require('path');
var app = express.createServer();
var db = require('diskdb');


//Change Express view folder based on where is the file that res.render() is called
app.set('views', path.join(__dirname, '/app'));

app.use(express.static(__dirname + '/app'));
app.use(express.bodyParser());
app.get('/', function (req, res) {
    res.render('index', {
        title: "Incubation Apps",
    });
});


var gcs = require(__dirname + '/app' + '/scripts/gcsRecognition');
var IbmRecognition = require(__dirname + '/app' + '/scripts/ibmRecognition');
var msb = require(__dirname + '/app' + '/scripts/msbRecognition');
var HINT_VOCAB = require(__dirname + '/app' + '/conf/hintVocab').hintVocab;

app.post('/recognize/gcs', function (req, res) {
    var datareceive = req.body.voice;
    var language = req.body.language;

    // voicedataArray is b64 encoded - as watson API need audio Stream
    // we need to convert b64 back to binary before wrapping with ReadStream
    var buffer = b64ToBinary(datareceive);
    // GG needs b64 dataEncoded
    gcs.recognition(buffer, language, HINT_VOCAB, true, function (response) {
        res.json(response);
    });
});

app.post('/recognize/ibm', function (req, res) {

    var datareceive = req.body.voice;
    var language = req.body.language;

    // voicedataArray is b64 encoded - as watson API need audio Stream
    // we need to convert b64 back to binary before wrapping with ReadStream
    var buffer = b64ToBinary(datareceive);
    // IBM Watson needs buffer
    var ibm = new IbmRecognition();
    ibm.recognition(buffer, language, HINT_VOCAB, function (response) {
        res.json(response);
    });

});


app.post('/recognize/msb', function (req, res) {
    var datareceive = req.body.voice;
    var language = req.body.language;

    let buffer = b64ToBinary(datareceive);
    // MS Speech Bing needs buffer
    msb.recognition(buffer, language, HINT_VOCAB, function (response) {
        res.json(response);
    })
});

app.post('/textAnalyse/ibm', function (req, res) {
    fs.readFile(req.files.file.path, {encoding: 'utf8'}, (err, data) => {
        if (err) {
            throw err;
        } else {
            var datareceive = data;
            var language = req.body.language;
            var ibm = new IbmRecognition();
            ibm.nluAnalyze(datareceive, language, function (response) {
                res.json(response);
            });
        }
    });
});

app.post('/textAnalyse/gcs', function (req, res) {
    fs.readFile(req.files.file.path, {encoding: 'utf8'}, (err, data) => {
        if (err) {
            throw err;
        } else {
            var datareceive = data;
            var language = req.body.language;
            gcs.nluAnalyze(datareceive, language, function (response) {
                res.json(response);
            });
        }
    });
});

app.post('/textAnalyse/msb', function (req, res) {
    fs.readFile(req.files.file.path, {encoding: 'utf8'}, (err, data) => {
        if (err) {
            throw err;
        } else {
            var datareceive = data;
            var language = req.body.language;
            msb.nluAnalyze(datareceive, language, function (response) {
                res.json(response);
            });
        }
    });
});

app.post('/textAnalyseText/ibm', function (req, res) {
    var datareceive = req.body.texte;
    var language = req.body.language;
    var ibm = new IbmRecognition();
    ibm.nluAnalyze(datareceive, language, function (response) {
        res.json(response);
    });
});

app.post('/textAnalyseText/gcs', function (req, res) {
    var datareceive = req.body.texte;
    var language = req.body.language;
    gcs.nluAnalyze(datareceive, language, function (response) {
        res.json(response);
    });
});

app.post('/textAnalyseText/msb', function (req, res) {
    var datareceive = req.body.texte;
    var language = req.body.language;
    msb.nluAnalyze(datareceive, language, function (response) {
        res.json(response);
    });
});

app.get('/passPhrases', function (req, res) {
    msb.speakerAllPassPhrases('en-us', function (response) {
        console.log("verificationProfile = " + JSON.stringify(response));
        res.json(response);
    });

})
app.post('/createVerificationProfile', function (req, res) {
    var identifiant = req.body.identifiant;
    let identity = retrieveIdentity({identifiant: identifiant})
    let verifProfile = null
    if (identity !== null) {
        verifProfile = identity.verificationProfile
    }
    var nomPrenom = req.body.nomPrenom;
    var voice = req.body.voice;
    let buffer = b64ToBinary(voice);
    msb.speakerVerificationProfile(buffer, verifProfile, function (response) {
        saveVerificationProfile(identifiant, nomPrenom, response.verificationProfileId, response.phrase)
        console.log("verificationProfile = " + response);
        res.json(response);
    });
});

app.post('/verificationProfile', function (req, res) {
    var voice = req.body.voice;
    var language = req.body.language
    var identifiant = req.body.identifiant
    if (identifiant.length > 0) {
    } else {
        res.json({transcript: "Identifiant vide"});
    }
    var voiceRecognitionProvider = req.body.voiceRecognitionProvider
    let buffer = b64ToBinary(voice);
    if (voiceRecognitionProvider === 'gcs') {
        gcs.recognition(buffer, language, HINT_VOCAB, false, function (response) {
            if (response.words === null || response.word === undefined) {

                // find VerificationProfile with identifiant variable
                // the call speakerVerification service to authticate the speaker
                let identite = retrieveIdentity({identifiant: identifiant});
                if (identite.verificationProfile === null) {
                    res.json({error: "L'identification vocale n'est pas disponible pour vous. Pour la rendre disponible, merci d'effectuer l'échantilonnage"})
                } else {
                    msb.speakerVerification(identite.verificationProfile, buffer, function (r) {
                        try {
                            let response = JSON.parse(r);
                            let verificationResult = response.result
                            let confidence = response.confidence
                            let phrase = response.phrase
                            let error = response.error
                            if (error) {
                                res.json({transcript: error});
                            } else if (confidence.toLowerCase() === 'high' || confidence.toLowerCase() === 'normal') {
                                if (verificationResult.toLowerCase() === 'accept') {
                                    // le service a  fait le match avec emprunt vocal --> OK
                                    let identity = retrieveIdentity({
                                        identifiant: identifiant,
                                        verificationProfile: identite.verificationProfile,
                                        phrase: phrase
                                    })
                                    if (identity !== null) {
                                        res.json({transcript: identity.nomPrenom})
                                    } else {
                                        res.json({transcript: "Echec d'authentification : votre identifiant ne correspond pas à votre emprunt vocal"});
                                    }
                                } else {
                                    res.json({transcript: "Echec d'authentification : votre identifiant ne correspond pas à votre emprunt vocal"});
                                }

                            } else {
                                res.json({transcript: "Je ne suis pas sûre de votre identité. Renouveler l'opération"});
                            }
                        }
                        catch (err) {
                            console.log(err)
                            res.json({transcript: "je ne sais pas vous authentifier : Erreur = " + err.message})
                        }

                    })
                }

            } else {
                res.json({transcript: 'Je ne sais pas reconnaître votre identifiant'})
            }
        });
    } else {
        res.json({transcript: 'Le fournisseur de service ' + voiceRecognitionProvider + " n'est pas supporté"})
    }
});

app.post('/retrieveIdentity', function (req, res) {
    let datareceive = req.body.identifiant;
    let identity = retrieveIdentity({identifiant: datareceive})
    if (identity === null) {
        res.json({error: 'Identifiant inconnu'})
    } else {
        if (identity.phrase === undefined) {
            res.json({error: "L'identification vocale n'est pas disponible pour vous. Pour la rendre disponible, merci d'effectuer l'échantilonnage"})
        } else {
            res.json({phrase: identity.phrase})
        }
    }
});

app.post('/personality/lettreDeMotivation', function (req, res) {
    let username = req.body.userName;
    let lettreDeMotivation = retrieveLettreDeMotivations(username);
    if (lettreDeMotivation != null && lettreDeMotivation.length > 0) {
        var ibm = new IbmRecognition();
        ibm.personalityInsight(lettreDeMotivation, function (data) {
            res.json({profile: data});
        });
    } else {
        res.json({error: 'Identifiant inconnu ou sans lettre de motivation'});
    }

})

app.post('/personality/rawText', function (req, res) {
    let textContent = req.body.content;

    var ibm = new IbmRecognition()

    ibm.personalityInsight(textContent, function (data) {

        res.json({profile: data});

    })

});

// your express configuration here

app.listen(3000, function () {
    console.log('Server started up and listening on port 3000')
})

function b64ToBinary(b64) {
    // voicedataArray is b64 encoded - as watson API need audio Stream
    // we need to convert b64 back to binary before wrapping with ReadStream
    var buffer;
    if (typeof Buffer.from === "function") {
        // Node 5.10+
        buffer = Buffer.from(b64, 'base64');
    } else {
        // older Node versions
        buffer = new Buffer(b64, 'base64');
    }
    return buffer;
}

function toNumeric(word) {
    switch (word) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            return word
        case 'zéro':
        case 'zero':
            return 0
        case 'un':
            return 1
        case 'deux':
            return 2
        case 'trois':
            return 3
        case 'quatre':
            return 4
        case 'cinq':
            return 5
        case 'six':
            return 6
        case 'sept':
            return 7
        case 'huit':
            return 8
        case 'neuf':
            return 9
    }
    return false;
}

function isZero(str) {
    let s1 = str.replace(/-/g, '')
    let s2 = s1.replace(/0/g, '')
    return s2.length === 0
}

function retrieveIdentity(criteria) {
    db = db.connect(__dirname + '/db', ['identities']);
    let identification = db.identities.find(criteria)
    if (identification !== null && identification !== undefined && identification.length > 0) {
        return identification[0]
    }
    return null
}

function retrieveLettreDeMotivations(idPoleEmploi) {
    db = db.connect(__dirname + '/db', ['lettre']);
    let candidat = db.lettre.find({identifiant: idPoleEmploi});

    if (candidat != null && candidat.length == 1) {
        console.log('Candidat trouvé ');
        return candidat[0].lettreDeMotivation;
    } else {
        console.log("aucun candidat n'a été trouvé");
        return null;
    }
}

function saveVerificationProfile(identifiant, nomPrenom, verificationProfile, phrase) {
    db = db.connect(__dirname + '/db', ['identities']);
    let identification = db.identities.find({identifiant: identifiant})
    if (identification !== null && identification.length > 0) {
        console.log('update identifiant : ' + identifiant + '; phrase = ' + phrase)
        db.identities.update({identifiant: identifiant}, {phrase: phrase})
    } else {
        console.log('create identifiant : ' + identifiant + '; verificationProfile=' + verificationProfile + '; phrase = ' + phrase)
        db.identities.save([{
            identifiant: identifiant,
            nomPrenom: nomPrenom,
            verificationProfile: verificationProfile,
            phrase: phrase
        }])
    }
}




