module.exports = {
    hintVocab: [
        'travail', 'emploi', 'domicile', 'région', 'métier', 'compétence', 'formation', 'boulanger', 'programmeur', 'développeur',
        'entreprise', 'salaire', 'CDI', 'CDD', 'intérim', 'temps partiel'
    ],

    personalityDico:
        [
            {"trait_id": "big5_openness", "english": "Openness", "francais": "Ouverture"},
            {"trait_id": "facet_adventurousness", "english": "Adventurousness", "francais": "Intrépidité"},
            {
                "trait_id": "facet_artistic_interests",
                "english": "Artistic interests",
                "francais": "Intérêts artistiques"
            },
            {"trait_id": "facet_emotionality", "english": "Emotionality", "francais": "Emotionnalité"},
            {"trait_id": "facet_imagination", "english": "Imagination", "francais": "Imagination"},
            {"trait_id": "facet_intellect", "english": "Intellect", "francais": "Intellect"},
            {"trait_id": "facet_liberalism", "english": "Authority-challenging", "francais": "Libéralisme"},
            {
                "trait_id": "big5_conscientiousness",
                "english": "Conscientiousness",
                "francais": "Conscience professionnelle"
            },
            {"trait_id": "facet_achievement_striving", "english": "Achievement striving", "francais": "Persévérance"},
            {"trait_id": "facet_cautiousness", "english": "Cautiousness", "francais": "Circonspection"},
            {"trait_id": "facet_dutifulness", "english": "Dutifulness", "francais": "Sens du devoir"},
            {"trait_id": "facet_orderliness", "english": "Orderliness", "francais": "Ordre"},
            {"trait_id": "facet_self_discipline", "english": "Self-discipline", "francais": "Autodiscipline"},
            {"trait_id": "facet_self_efficacy", "english": "Self-efficacy", "francais": "Efficacité"},
            {"trait_id": "big5_extraversion", "english": "Extraversion", "francais": "Extraversion"},
            {"trait_id": "facet_activity_level", "english": "Activity level", "francais": "Niveau d'activité"},
            {"trait_id": "facet_assertiveness", "english": "Assertiveness", "francais": "Affirmation de soi"},
            {"trait_id": "facet_cheerfulness", "english": "Cheerfulness", "francais": "Gaieté"},
            {
                "trait_id": "facet_excitement_seeking",
                "english": "Excitement-seeking",
                "francais": "Recherche de sensations"
            },
            {"trait_id": "facet_friendliness", "english": "Outgoing", "francais": "Cordialité"},
            {"trait_id": "facet_gregariousness", "english": "Gregariousness", "francais": "Convivialité"},
            {"trait_id": "big5_agreeableness", "english": "Agreeableness", "francais": "Amabilité"},
            {"trait_id": "facet_altruism", "english": "Altruism", "francais": "Altruisme"},
            {"trait_id": "facet_cooperation", "english": "Cooperation", "francais": "Coopération"},
            {"trait_id": "facet_modesty", "english": "Modesty", "francais": "Modestie"},
            {"trait_id": "facet_morality", "english": "Uncompromising", "francais": "Moralité"},
            {"trait_id": "facet_sympathy", "english": "Sympathy", "francais": "Empathie"},
            {"trait_id": "facet_trust", "english": "Trust", "francais": "Confiance"},
            {"trait_id": "big5_neuroticism", "english": "Emotional range", "francais": "Emotivité"},
            {"trait_id": "facet_anger", "english": "Fiery", "francais": "Colère"},
            {"trait_id": "facet_anxiety", "english": "Prone to worry", "francais": "Anxiété"},
            {"trait_id": "facet_depression", "english": "Melancholy", "francais": "Dépréssion"},
            {"trait_id": "facet_immoderation", "english": "Immoderation", "francais": "Immodération"},
            {"trait_id": "facet_self_consciousness", "english": "Self-consciousness", "francais": "Susceptibilité"},
            {"trait_id": "facet_vulnerability", "english": "Susceptible to stress", "francais": "Vulnérabilité"},

            {"trait_id": "need", "english": "needs", "francais": "Besoins"},
            {"trait_id": "need_challenge", "english": "Challenge", "francais": "Combativité"}, //
            {"trait_id": "need_closeness", "english": "Closeness", "francais": "Promixité"}, //
            {"trait_id": "need_curiosity", "english": "Curiosity", "francais": "Curiosité"},
            {"trait_id": "need_excitement", "english": "Excitement", "francais": "Enthousiasme"},//
            {"trait_id": "need_harmony", "english": "Harmony", "francais": "Harmonie"}, //
            {"trait_id": "need_ideal", "english": "Ideal", "francais": "Idéal"}, //
            {"trait_id": "need_liberty", "english": "Liberty", "francais": "Liberté"}, //
            {"trait_id": "need_love", "english": "Love", "francais": "Amour"}, //
            {"trait_id": "need_practicality", "english": "Practicality", "francais": "Pragmatisme"},
            {"trait_id": "need_self_expression", "english": "Self-expression", "francais": "Extériorisation"}, //
            {"trait_id": "need_stability", "english": "Stability", "francais": "Besoin de concret"}, //Concret
            {"trait_id": "need_structure", "english": "Structure", "francais": "Besoin de stabilité"},

            {"trait_id": "value", "english": "values", "francais": "Valeurs"},
            {"trait_id": "value_conservation", "english": "Conservation", "francais": "Conservatisme"}, //
            {
                "trait_id": "value_openness_to_change",
                "english": "Openness to change",
                "francais": "Ouverture au changement"
            }, //
            {"trait_id": "value_hedonism", "english": "Hedonism", "francais": "Hédonisme"}, //
            {"trait_id": "value_self_enhancement", "english": "Self-enhancement", "francais": "Ambition personnelle"},
            {"trait_id": "value_self_transcendence", "english": "Self-transcendence", "francais": "Dépassement de soi"}
        ]





}

