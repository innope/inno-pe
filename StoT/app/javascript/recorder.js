var WORKER_PATH = './javascript/recorderWorker.js';

var Recorder = function(source, cfg){
  var config = cfg || {};
  var bufferLen = config.bufferLen || 4096;

  // Si le Recorder est instancié avec une source == null (cas du chargement d'un fichier audio au lieu d'un enregistrement direct
  if (source !== null) {
      this.context = source.context;
      this.node = (this.context.createScriptProcessor ||
          this.context.createJavaScriptNode).call(this.context,
          bufferLen, 2, 2);
  }

  var worker = new Worker(WORKER_PATH);
  worker.onmessage = function(e){
    var blob = e.data;
    currCallback(blob);
  }

  worker.postMessage({
    command: 'init',
    config: {
      // Si pas de contexte (cas d'un chargement d'un fichier Wav au lieu d'eun enregistrement en direct),
        // on met le saple rate à 44100 par défaut
      sampleRate: (this.context === undefined || this.context === undefined )? 44100:this.context.sampleRate
    }
  });

  var recording = false,
    currCallback;

    // Si pas de contexte (cas d'un chargement d'un fichier Wav au lieu d'eun enregistrement en direct),
    // Il n'y aura pas d'événement onaudioprocess
  if (this.node !== null && this.node !== undefined) {
      this.node.onaudioprocess = function (e) {
          if (!recording) return;
          worker.postMessage({
              command: 'record',
              buffer: [
                  e.inputBuffer.getChannelData(0),
                  e.inputBuffer.getChannelData(1)
              ]
          });
      }
  }

  this.configure = function(cfg){
    for (var prop in cfg){
      if (cfg.hasOwnProperty(prop)){
        config[prop] = cfg[prop];
      }
    }
  }

  this.record = function(){
    recording = true;
  }

  this.stop = function(source){
    recording = false;
    this.node.disconnect();
    source.disconnect();
  }

  this.clear = function(){
    worker.postMessage({ command: 'clear' });
  }

  this.getBuffer = function(cb) {
    currCallback = cb || config.callback;
    worker.postMessage({ command: 'getBuffer' })
  }

  this.exportWAV = function(cb, type){
    currCallback = cb || config.callback;
    type = type || config.type || 'audio/wav';
    if (!currCallback) throw new Error('Callback not set');
    worker.postMessage({
      command: 'exportWAV',
      type: type
    });
  }
  // méthode spécifique pour réencoder un wav orginal en mono 16000Hz
    this.exportWAVWithBuffer = function(cb, type, rbl, rbr, rl, sr){
      currCallback = cb || config.callback;
      type = type || config.type || 'audio/wav';
      if (!currCallback) throw new Error('Callback not set');
      worker.postMessage({
        command: 'exportWAVWithBuffer',
        type: type,     // audio/wav
          rbl: rbl,     // buffer audio gauche
          rbr: rbr,     // buffer audio droit
          rl: rl,       // longueur buffer audio
          sr: sr        // sample rate du buffer audio original
      });
    }

    this.exportWAVSpecificSampleRate = function(cb, specificSampleRate, type){
      currCallback = cb || config.callback;
      type = type || config.type || 'audio/wav';
      if (!currCallback) throw new Error('Callback not set');
      worker.postMessage({
        command: 'exportWAVSpecificSampleRate',
        type: type,
        specificSampleRate: specificSampleRate
      });
    }

    if (source !== null && source !== undefined) {
        source.connect(this.node);
        this.node.connect(this.context.destination);    //this should not be necessary
    }
};
