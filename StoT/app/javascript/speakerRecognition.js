// set up basic variables for app
var record = document.querySelector('#recordButton');
var recordIdentification = document.querySelector('#recordIdentification');
var audio = document.querySelector('#myaudio');
var audioIdentification = document.querySelector('#myAudioIdentification');
var canvas = document.querySelector('.visualizer');
var canvasIdentification = document.querySelector('#canvasIdentification');
var spanmicro = document.querySelector('#spanmicro');
var enrollVoice;

if (navigator.mediaDevices.getUserMedia) {
    console.log('getUserMedia supported.');

    var constraints = {audio: true};
    var onSuccess = function (stream) {

        //var mediaRecorder = new MediaRecorder(stream);


        record.onclick = function () {
            var recordimage = document.getElementById("recordimage");
            let associerButton = document.getElementById("associerButton");
            var actualImg = recordimage.getAttribute("xlink:href");
            if (actualImg.indexOf("images/002-microphone.svg") >= 0) {

                 stopRecording(audio, (b64) => {
                     enrollVoice = b64
                    console.log("recorder stopped");
                    actualImg = "images/001-muted.svg"
                    recordimage.setAttribute("xlink:href", actualImg);
                    associerButton.disabled = false;
                }, true);
            } else {
                actualImg = "images/002-microphone.svg";
                recordimage.setAttribute("xlink:href", actualImg);

                // Démarrer l'enregistrement
                resetResult();
                //mediaRecorder.start();
                startRecording(stream);
                visualize(stream, canvas);
                //console.log(mediaRecorder.state);
                console.log("recorder started");
            }

        };
        recordIdentification.onclick = function () {
            var recordimage = document.getElementById("recordIdentificationImage");
            var actualImg = recordimage.getAttribute("xlink:href");
            if (actualImg.indexOf("images/002-microphone.svg") >= 0) {

                stopRecording(audioIdentification, (b64) => {
                    showIdentification(b64);
                    console.log("recorder identification stopped");
                    actualImg = "images/001-muted.svg"
                    recordimage.setAttribute("xlink:href", actualImg);
                }, true);
            } else {
                actualImg = "images/002-microphone.svg";
                recordimage.setAttribute("xlink:href", actualImg);

                // Démarrer l'enregistrement
                resetResult();
                startRecording(stream);
                visualize(stream, canvasIdentification);
                console.log("recorder identification started");
            }

        };
    }

    var onError = function (err) {
        alert('The following error occured: ' + err);
        console.log('The following error occured: ' + err);
    }

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

} else {
    console.log('getUserMedia not supported on your browser!');
}


window.onresize = function () {
    canvas.width = spanmicro.offsetWidth - 10;
}

window.onresize();

function associerCompteVoix(idAudio, idIdentifiant, nP) {
    if (enrollVoice === null) {
        alert("Aucun échantillon de voix n'est enregistré ni transmis pour l'association");
        return;
    }
    document.getElementById('spinnerAssociation').setAttribute("style", "visibility: visible");
    let identifiant = document.getElementById(idIdentifiant).value;
    let nomPrenom = document.getElementById(nP).value;

    callAssociationService(identifiant, nomPrenom, enrollVoice)

}

function callAssociationService(identifiant, nomPrenom, vdata) {
    $.ajax({
        type: "POST",
        url: '/createVerificationProfile',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(
            {identifiant: identifiant, nomPrenom: nomPrenom, voice: vdata}),
        success: function (data) {
            document.getElementById('spinnerAssociation').setAttribute("style", "visibility: hidden");
            if (data.phrase) {
            alert("L'échantilonnage de votre voix avec la phrase '" + data.phrase + "' est terminé. Cet échantillonage est associé à votre identifant " + identifiant);
            } else {
                alert("Echec de l'échantilonnage.Merci de renouveler l'opération.")
            }
        },
        error: function (xhr, status, error) {
            document.getElementById('spinnerAssociation').setAttribute("style", "visibility: hidden");
            alert(status + ' - ' + error.message)
        }
    })
}


// Reset stars, rating value & result text before new recognition
function resetResult() {
    document.getElementById('spinner').setAttribute("style", "visibility: hidden");
    document.getElementById("transcript").value = "";
}

function showError(xhr, status, error) {
    // Cacher le spinner et afficher le bloc de confiance
    document.getElementById('spinner').setAttribute("style", "visibility: hidden");
    var message = "";
    if (xhr.readyState === 4) {
        message = status;
    }
    else if (xhr.readyState === 0) {
        message = "Erreur de connexion au serveur";
    }
    else {
        message = "Une erreur est survenue - Code = " + xhr.readyState;
    }
    document.getElementById('transcript').value = message;
}

function stotByFile() {
    loadWav("myaudio", "voiceWav", (b64 => {
            enrollVoice = b64;
            let associerButton = document.getElementById('associerButton');
            associerButton.disabled = false;
        }
    ));
}

function showIdentification(vdata) {
    document.getElementById('spinner').setAttribute("style", "visibility: hidden");
    var e = document.getElementById("language");
    var identifiant = document.getElementById("identifiantCheck").value;
    var language = 'en-us' //e.options[e.selectedIndex].value;
    callIdentificationProfile('gcs',vdata, language, identifiant, function (data) {
        document.getElementById('transcript').value = "Bonjour " + data.transcript;
    }, function(xhr, status, error) {
        showError(xhr, status, error);
    })
}

function callIdentificationProfile(challenger, vdata, languageCode, identifiant, callbackOK, callbackKO) {
    //Send data to server
    $.ajax({
        type: "POST",
        url: '/verificationProfile',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(
            {identifiant: identifiant, voice: vdata, language: languageCode, voiceRecognitionProvider: challenger}),
        success: function (data) {
            callbackOK(data)
        },
        error: function (xhr, status, error) {
            callbackKO(xhr, status, error);
        }
    });
}

function callRetrieveIdentity(value) {
    if (value.length >= 6) {
        $.ajax({
                type: "POST",
                url: '/retrieveIdentity',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(
                    {identifiant: value}),
                success: function (data) {
                    if (data.error === null || data.error === undefined) {
                        document.getElementById('identifiantPhrase').value=data.phrase;
                        document.getElementById('divIdentifiantPhrase').setAttribute("style", "visibility: visible");
                        document.getElementById('identifiantPhrase').focus()
                    } else {
                        document.getElementById('identifiantPhrase').value='';
                        document.getElementById('divIdentifiantPhrase').setAttribute("style", "visibility: hidden");
                        document.getElementById('transcript').value = data.error;
                    }
                },
                error: function (xhr, status, error) {
                    callbackKO(xhr, status, error);
                }
            });
    } else {
        // hide authentication pass phrase
        document.getElementById('identifiantPhrase').value='';
        document.getElementById('divIdentifiantPhrase').setAttribute("style", "visibility: hidden");
    }
}

function fillPassPhrases(passPhraseListId) {
    $.ajax({
            type: "get",
            url: '/passPhrases',
            success: function (data) {
                if (data.error) {
                    let passPhraseList = '<li class="mdl-menu__item" data-val="">' + data.error + '</li>'
                    document.getElementById(passPhraseListId).innerHTML(passPhraseList)
                } else {
                    let passPhraseListModel ='<li class="mdl-menu__item"  data-val=""></li>';
                    let passPhraseList=''
                    for (let i=0;i<data.length;i++) {
                        passPhraseList += (passPhraseListModel.replace('</li>', data[i].phrase) + '</li>').replace('data-val=""', 'data-val="' + i + '"')
                    }
                    alert(passPhraseList)
                    document.getElementById(passPhraseListId).innerHTML = passPhraseList
                }
            },
            error: function (xhr, status, error) {
                let passPhraseList = '<li class="mdl-menu__item" data-val="">' + error.message + '</li>'
                document.getElementById(passPhraseListId).innerHTML(passPhraseList)
            }
        });

}





