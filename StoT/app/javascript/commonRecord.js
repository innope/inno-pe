function loadWav(idAudio, idFileObject, callback) {
    // décodage du WAV pour conversion en mono / 16000Hz
    var readerWav = new FileReader();
    readerWav.readAsArrayBuffer(document.getElementById(idFileObject).files[0]);
    readerWav.onloadend = function () {
        var base64data = readerWav.result;
        var ac = new (window.AudioContext || window.webkitAudioContext)();
        // Décodage
        ac.decodeAudioData(base64data, function (buffer) {
            var recorder = new Recorder(null)

            //conversion et réexpoter en wav mono 16000Hz
            recorder.exportWAVWithBuffer(function (blob) {
                    var url = URL.createObjectURL(blob);

                    // Charger le fichier converti dans le lecteur audio HTML
                    if (audio !== null && audio !== undefined) {
                        audio.src = url;
                        audio.load()
                    }

                    // Encoder le WAV converti en base 64
                    var readerWav = new FileReader();
                    readerWav.readAsDataURL(blob);
                    readerWav.onloadend = function () {
                        var base64data = readerWav.result;
                        var b64 = base64data.split('base64,')[1];
                            callback(b64);
                    }
                }, 'audio/wav', buffer.getChannelData(0), buffer.getChannelData(0), buffer.getChannelData(0).length, buffer.sampleRate)
        })
    }

    // readerWav.readAsDataURL(document.getElementById(idFileObject).files[0]);
    // readerWav.onloadend = function () {
    //     var base64data = readerWav.result;
    //     var b64 = base64data.split('base64,')[1];
    //     // Envoyer l'enregistrement WAV encodé en base64 au serveur pour la transcription
    //     callback(b64);
    // }
}

var recorder;
var input;
var audioCtx = new (window.AudioContext || webkitAudioContext)();

var stopRecord = true;

function visualize(stream, canvas) {
    var canvasCtx = canvas.getContext("2d");
    var source = audioCtx.createMediaStreamSource(stream);

    var analyser = audioCtx.createAnalyser();
    analyser.fftSize = 2048;
    var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);

    input.connect(analyser);
    //analyser.connect(audioCtx.destination);

    draw()

    function draw() {
        if (stopRecord) {
            input.disconnect();
            return;
        }
        WIDTH = canvas.width;
        HEIGHT = canvas.height;

        requestAnimationFrame(draw);

        analyser.getByteTimeDomainData(dataArray);

        //canvasCtx.fillStyle = "rgba(255, 255, 255, 0.5)";
        canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(0, 0, 255)';

        canvasCtx.beginPath();

        var sliceWidth = WIDTH * 1.0 / bufferLength;
        var x = 10;


        for (var i = 0; i < bufferLength; i++) {

            var v = dataArray[i] / 128.0;
            var y = v * HEIGHT / 2;

            if (i === 0) {
                canvasCtx.moveTo(x, y);
            } else {
                canvasCtx.lineTo(x, y);
            }

            x += sliceWidth;
        }

        canvasCtx.lineTo(canvas.width, canvas.height / 2);
        canvasCtx.stroke();

    }
}

function startRecording(stream) {
    stopRecord = false;
    input = audioCtx.createMediaStreamSource(stream);
    recorder = new Recorder(input);
    recorder && recorder.record();
}


function stopRecording(audio, callback) {
    stopRecord = true;
    recorder && recorder.stop(input);
    recorder && recorder.exportWAV(function (blob) {
        var url = URL.createObjectURL(blob);
        if (audio !== null && audio !== undefined) {
            audio.src = url;
        }
        // Encoder le WAV en base 64
        var readerWav = new FileReader();
        readerWav.readAsDataURL(blob);
        readerWav.onloadend = function () {
            var base64data = readerWav.result;
            var b64 = base64data.split('base64,')[1];
                recorder.clear();
                callback(b64);
        }
    }, 16000, null);
}

function callRecognitionService(challenger, vdata, languageCode, callbackOK, callbackKO) {
    //Send data to server
    $.ajax({
        type: "POST",
        url: '/recognize/' + challenger,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(
            {voice: vdata, language: languageCode}),
        success: function (data) {
            callbackOK(challenger, data)
        },
        error: function (xhr, status, error) {
            callbackKO(challenger, xhr, status, error);
        }
    });
}


