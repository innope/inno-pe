let textToanalyse = document.getElementById('textPersoInput');
let sunburstChart = document.getElementById('datashow');
let idPoleEmploi = document.getElementById('idPE');
let errorToast = document.getElementById('error-toast');
let maxTraitchart = document.getElementById('maxTraitsChart');

let personalityPEFilter = [
    {
        "id_metier": "BOU",
        "label": "Boulanger",
        "traits": [
            {"axis": 'Ouverture', "value": 0.25},
            {"axis": 'Sens du devoir', "value": 0.55},
            {"axis": 'Extraversion', "value": 0.85},
            {"axis": 'Coopération', "value": 0.25},
            {"axis": 'Cordialité', "value": 0.65}
        ]

    }, {
        "id_metier": "ING",
        "label": "Ingénieur",
        "traits": [
            {"axis": 'Efficacité', "value": 0.75},
            {"axis": 'Confiance', "value": 0.95},
            {"axis": 'Intellect', "value": 0.75},
            {"axis": 'Imagination', "value": 0.85},
            {"axis": 'Cordialité', "value": 0.45}
        ]

    },{
        "id_metier": "JOU",
        "label": "Journaliste",
        "traits": [
            {"axis": 'Intérêts artistiques', "value": 0.75},
            {"axis": 'Autodiscipline', "value": 0.95},
            {"axis": 'Intrépidité', "value": 0.85},
            {"axis": 'Niveau d\'activité', "value": 0.85},
            {"axis": 'Emotionnalité', "value": 0.20}
        ]

    }

];

let needsValuesFilter = [
    {"trait_id": "need_curiosity", "legende": "A le désir de découvire, de trouver, de grandir.", "axis": "Curiosité"},
    {
        "trait_id": "need_practicality",
        "legende": "A le désir d'efficacité et de compétence afin de faire un travail.",
        "axis": "Pragmatisme"
    },
    {
        "trait_id": "need_stability",
        "legende": "Favorise le sensible, l'éprouvé et le testé.",
        "axis": "Besoin de concret"
    },
    {
        "trait_id": "need_structure",
        "legende": "A le désir de garder les choses organisées et sous contrôles.",
        "axis": "Besoin de stabilité"
    },
    {"trait_id": "value_self_enhancement", "legende": "Recherche le succès personnel.", "axis": "Ambition personnelle"},
    {
        "trait_id": "value_self_transcendence",
        "legende": "Concerné par l'épanouissement des autres et leurs intérêts.",
        "axis": "Dépassement de soi"
    }, {"trait_id": "big5_openness", "legende": "Ouvert à l'expérience de nouvelles activités", "axis": "Ouverture"},
    {
        "trait_id": "big5_conscientiousness",
        "legende": "Agit de manière organisée et pensée",
        "axis": "Conscience professionnelle"
    },
    {"trait_id": "big5_extraversion", "legende": "Est stimulé par la companie des autres", "axis": "Extraversion"},
    {
        "trait_id": "big5_agreeableness",
        "legende": "A tendance à être compatissante et coopérative envers les autres ",
        "axis": "Amabilité"
    },
    {"trait_id": "big5_neuroticism", "legende": "Sensible à son environement", "axis": "Emotivité"}
];


let metierSelectionner;
let personalityData;
let progressBar;

progressBar = document.getElementById('progressBar');
var wordCount;

/** Compteur de mot pour le texte area */
$(document).ready(function () {
    $("#textPersoInput").on('keyup', function () {
        wordCount = this.value.match(/\S+/g).length;
        $('#wordCount').text(wordCount);

    });

    var listMetier = $('#offreEmploiListId')

    personalityPEFilter.forEach(function (item) {
        let li = document.createElement("li");
        li.setAttribute("class", "mdl-menu__item");
        li.setAttribute("data-val", item.id_metier);
        li.setAttribute("tabindex", "-1")
        li.appendChild(document.createTextNode(item.label));
        listMetier[0].appendChild(li);
    })

    listMetier.click(function () {
        metierSelectionner = personalityPEFilter.filter(function (o) {
            return o.id_metier === $('#emploiValue')[0].value
        })[0];

        if (personalityData != null) printRadarChartMetier(personalityData)
    });

});

function analysePersonality() {
    if(idPoleEmploi.value.length>0){
        personalityCallLetter(idPoleEmploi.value);
    }else if (wordCount > 100){
        personalityCallText(textToanalyse.value);
    }else{
        let data ={message: 'Merci de renseigner soit un Id pole emploi ou un texte de plus de 100 mots'}
        errorToast.MaterialSnackbar.showSnackbar(data);
    }

}

personalityCallSucessHandler = function (data) {
    if (data) {
        if (data.error === null || data.error === undefined) {
            personalityData = data.profile;
            printRadarChartBigFive(personalityData)
            if(metierSelectionner !== undefined && metierSelectionner!==null){
                printRadarChartMetier(personalityData);
            }
            document.getElementById("showCompletProfil").style.display = "inline-block";
        }
        else {
            alert(data.error);
        }
        progressBar.style.display = "none";
    }
}

function personalityCallLetter(userName) {
    let payload = {
        userName: userName
    };
    progressBar.style.display = "inherit";
    $.ajax({
        type: 'POST',
        url: '/personality/lettreDeMotivation',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(payload),
        success: personalityCallSucessHandler,
        error: function (xhr, status, error) {
            console.log(xhr, status, error);
            progressBar.style.display = "none";
        }
    });
}

function personalityCallText(text) {

    let payload = {
        content: text,
        contenttype: 'text/plain;charset=utf-8',
        language: 'en'
    };
    progressBar.style.display = "inherit";
    $.ajax({
            type: 'POST',
            url: '/personality/rawText',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(payload),
            success: personalityCallSucessHandler,
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
                progressBar.style.display = "none";
            }

        }
    )

}

function printPersonalityScheme(data) {
    if (data) {
        if (data.error === null || data.error === undefined) {

            let chart = new PersonalitySunburstChart({
                'element': sunburstChart,
                'version': 'v3'
            });
            chart.show(data);
        } else {
            console.log("Oups something wrong append during callback");
        }
    }
}

function printRadarChartBigFive(profile) {
    let resultBigFive = [];
    let resultValue = [];
    let resultProfile = [];
    //Trie la liste de résultat par odre croissant selon la distance
    profile.personality.forEach(x => resultBigFive.push(plotItem(x.name, x.percentile)));

    // 1 filter : Selectionner seulement les valeurs présents dans la Liste needsValuesFilter
    // 2 forEach : Pour ces valeurs, les placer dans le liste resultValue sous la forme {"axis" : a, "value" : v}
    profile.values.filter(value => needsValuesFilter.some(x => x.trait_id === value.trait_id)).forEach(x => resultValue.push(plotItem(x.name, x.percentile)));
    profile.needs.filter(value => needsValuesFilter.some(x => x.trait_id === value.trait_id)).forEach(x => resultValue.push(plotItem(x.name, x.percentile)));


    let radarChartOptions = {
        w: width,
        h: height,
        margin: margin,
        maxValue: 0.5,
        levels: 5,
        roundStrokes: false,
        color: colorBlank,
        opacityArea: 0.1, 	//The opacity of the area of the blob
        showAxis: true, // print the axis line
        closeLines: false, // Ferme la figure
        legend: ['Personalité instrinsèque', 'Besoins et motivations']
    };


    RadarChart("#maxTraitsChart", [resultBigFive, resultValue], radarChartOptions, needsValuesFilter);

}

function printRadarChartMetier(profile) {
    let resultMetier = [];
    traverse(profile, processMetier, resultMetier);


    let radarChartOptions = {
        w: width,
        h: height,
        margin: margin,
        maxValue: 0.5,
        levels: 5,
        roundStrokes: true,
        color: color,
        opacityArea: 0.35, 	//The opacity of the area of the blob
        showAxis: true, // print the axis line
        closeLines: true,
        legend: ['Profil analysé','Profil metier :'+ metierSelectionner.label]
    };

    metierSelectionner.traits.sort((a,b) => a.axis.localeCompare(b.axis));
    resultMetier.sort((a,b) => a.axis.localeCompare(b.axis));

    RadarChart("#radarChart", [resultMetier,metierSelectionner.traits], radarChartOptions);
}



$('#showCompletProfil').click(function () {
    printPersonalityScheme(personalityData);
    dialog.showModal();

});

var dialog = document.querySelector('dialog');
if (!dialog.showModal) {
    dialogPolyfill.registerDialog(dialog);
}
dialog.querySelector('.close').addEventListener('click', function () {
    dialog.close();
});
var margin = {top: 100, right: 100, bottom: 100, left: 100},
    width = Math.min(800, window.innerWidth - 10) - margin.left - margin.right,
    height = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);

var color = d3.scale.ordinal()
    .range(["#EDC951", "#CC333F", "#00A0B0"]);

var colorBlank = d3.scale.ordinal().range(["#0051ec", "#eebd4a", "#21f000"])

//called with every property and its value
function processMetier(key, obj) {
    if (key === "trait_id" && metierSelectionner.traits.some(trait => trait.axis === obj['name'])) {
        return plotItem(obj['name'], roundDecimal(obj['percentile']));
    }
    return null;
}


function traverse(o, func, agregate) {
    for (let i in o) {
        let token = func.apply(this, [i, o])
        if (token !== undefined && token !== null) agregate.push(token);
        if (o[i] !== null && typeof(o[i]) == "object") {
            //going one step down in the object tree!!
            traverse(o[i], func, agregate);
        }
    }
}

function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

function roundDecimal(number) {
    return precisionRound(number,2);
}


function plotItem(axis, value) {
    return {"axis": axis, "value": value};
}