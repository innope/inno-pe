var request = require('request');
var sox = require('sox');
var BingSpeechClient = require('bingspeech-api-client').BingSpeechClient;
var bingSpeechClient = new BingSpeechClient("45f15ceb25c34b6b93857412cb3d969f");
var textAnalyseConnectParam = {endpoint: "https://westus.api.cognitive.microsoft.com", apiKey: "4cc42187bdf64e52ae5cc41cd93bc3e3"};
var speakerRecognitionConnectParam = {endpoint: "https://westus.api.cognitive.microsoft.com", apiKey: '44bcbe19a419459b803f918bbf4a304d'};


module.exports = {
    recognition: function msbRecognition(buffer, languageCode, hintVocab, callback) {
        bingSpeechClient.recognize(buffer, languageCode)
            .then(response => {
                    if (response.results !== null && response.results !== undefined && response.results.length > 0) {
                        console.log("<Microsoft Speecg Bing> Transcription : " + response.results[0].name);
                        var lang = languageCode.split('-')[0];
                        const docs = {
                            documents: [
                                {
                                    language: lang,
                                    id: "1",
                                    text: response.results[0].name
                                }
                            ]
                        };

                        callSentimentAnalyze(textAnalyseConnectParam.endpoint, textAnalyseConnectParam.apiKey, docs, function (sentimentResult) {
                            if (sentimentResult === undefined) {
                                callback({transcript: "<Microsoft Speecg Bing> : Je ne sais pas transcrire"});
                                return
                            }
                            var sent = interpretSentiment(sentimentResult.documents[0]);
                            callback({
                                transcript: response.results[0].name,
                                confidence: response.results[0].confidence,
                                words: response.results[0].lexical,
                                tone: sent
                            });
                        })
                    } else {
                        callback({transcript: "<Microsoft Speecg Bing> : Je ne sais pas transcrire"});
                    }
                }
            )
            .catch((err) => {
                console.error('<Microsoft Speecg Bing> : ERROR:', err);
                callback({transcript: "<Microsoft Speecg Bing> : Je ne sais pas transcrire (Erreur = " + err + ")"});
            });
    },
    nluAnalyze: function nluAnalyze(document, languageCode, callback) {
        var lang = languageCode.split('-')[0];
        const docs = {
            documents: [
                {
                    language: lang,
                    id: "1",
                    text: document
                }
            ]
        };
        callSentimentAnalyze(textAnalyseConnectParam.endpoint, textAnalyseConnectParam.apiKey, docs, function (sentimentResult) {
            if (sentimentResult === undefined || sentimentResult.documents === undefined || sentimentResult.documents === null) {
                callback({transcript: "<Microsoft Speecg Bing> : Je ne sais pas transcrire"});
                return
            }
            var sent = interpretSentiment(sentimentResult.documents[0]);
            callback({
                transcript: document,
                tone: sent
            });
        })
    },
    speakerAllPassPhrases: function speakerAllPassPhrases (language, callback) {
        getAllPassPhrases(speakerRecognitionConnectParam.endpoint, speakerRecognitionConnectParam.apiKey, language,
            (response) => {callback(response)})
    },
    speakerVerificationProfile: function speakerCreateIdentificationProfile(voice, verifProfile, callback) {
        createAndEnrollIdentificationProfile(speakerRecognitionConnectParam.endpoint, speakerRecognitionConnectParam.apiKey, voice, verifProfile,
            function (response) {
                callback(response);
            });
    },
    speakerVerification: function speakerIdentification(verificationProfile, voice, callback) {
        verification(speakerRecognitionConnectParam.endpoint, speakerRecognitionConnectParam.apiKey, verificationProfile, voice,
            function (response) { callback(response)});
    },
}

function interpretSentiment(sentiment) {
    if (sentiment === null) {
        return {emotions: {neutral: 0.1}}
    }
    var score = sentiment.score;

    if (score > 0) {
        if (score <= 0.3) {
            // colère
            return {emotions: {anger: 0.9}}
        } else if (score <= 0.45) {
            // dégoûté
            return {emotions: {disgust: 0.9}}
        } else if (score <= 0.55) {
            // peur
            return {emotions: {fear: 0.9}}
        } else if (score <= 0.7) {
            // maussade
            return {emotions: {sadness: 0.9}}
        } else {
            // neutre
            return {emotions: {joy: 0.9}}
        }
    }
    else {
        // neutre
        return {emotions: {neutral: 0.9}}
    }
}

function callSentimentAnalyze(endPoint, apiKey, documents, callback) {
    var accessKey = apiKey;
    var uri = endPoint;
    var path = '/text/analytics/v2.0/sentiment';

    let request_params = {
        headers: {
            'Ocp-Apim-Subscription-Key': accessKey,
        },
        body: JSON.stringify(documents)
    };

    request.post(uri + path, request_params, (err, resp, body) => {
        if (err) {
            console.log(err.message);
            throw (err);
        }
        let body_ = JSON.parse(body);
        let body__ = JSON.stringify(body_, null, '  ');
        callback(body_);
    })
}

function getAllPassPhrases(endPoint, accessKey, language, callback) {
    var path = '/spid/v1.0/verificationPhrases?locale=' + language

    let request_params = {
        headers: {
            'Ocp-Apim-Subscription-Key': accessKey,
            'Content-Type': 'application/json',
        }
    };
    request.get(endPoint + path, request_params, (err, resp, b) => {
        try {
            let body = JSON.parse(b);
            callback(body)
        } catch (err) {
            callback({error : err.message})
        }
    })
}
function createAndEnrollIdentificationProfile(endPoint, accessKey, voice, verifProfile, callback) {
    if (verifProfile === null) {
        var path = '/spid/v1.0/verificationProfiles'; //identificationProfiles';

        let request_params = {
            headers: {
                'Ocp-Apim-Subscription-Key': accessKey,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({locale: "en-us",})
        };

        // call create identificationProfile service
        request.post(endPoint + path, request_params, (err, resp, body) => {
            if (err) {
                console.log(err.message);
                throw (err);
            }
            let body_ = JSON.parse(body);
            if (body_.verificationProfileId === undefined || body_.verificationProfileId === null) {
                console.log('Error identificationProfile creation service');
                throw (new Error('Error identificationProfile creation service'));
            }
            // enroll the voice with identificationProfileId
            enrollVerificationProfile(endPoint, accessKey, body_.verificationProfileId, voice, function (response) {
                callback(response)
            })
        })
    } else {
        // if the verificationProfileId is already exist then reset it and re-enroll
        resetEnrollment(endPoint, accessKey, verifProfile, function(r) {
            if (r === 'OK') {
                enrollVerificationProfile(endPoint, accessKey, verifProfile, voice, function (response) {
                    callback(response)
                })
            } else {
                callback({error: "Impossible de modifier l'échantilonnage"})
            }
        })
    }
}

function resetEnrollment(endPoint, accessKey, verificationProfileId, callback) {
    var path = '/spid/v1.0/verificationProfiles/' + verificationProfileId + '/reset';

    let request_params = {
        headers: {
            'Ocp-Apim-Subscription-Key': accessKey,
        }
    };
    request.post(endPoint + path, request_params, (err, resp, b) => {
        if (resp.statusCode === 200) {
            callback('OK')
        } else {
            callback('KO')
        }
    })
}
function enrollVerificationProfile(endPoint, accessKey, verificationProfileId, voice, callback) {
//    var path = '/spid/v1.0/identificationProfiles/' + identificationId + '/enroll?shortAudio=true';
    var path = '/spid/v1.0/verificationProfiles/' + verificationProfileId + '/enroll?shortAudio=true';

    let request_params = {
        headers: {
            'Ocp-Apim-Subscription-Key': accessKey,
        },
        body: voice
    };

    // Call enrollment service
    request.post(endPoint + path, request_params, (err, resp, b) => {
        let body = JSON.parse(b)
        if (resp.statusCode === 200) {
            if (body.enrollmentStatus.toLowerCase() === 'enrolled') {
                    callback({verificationProfileId: verificationProfileId, phrase: body.phrase})
            } else {
                if (body.remainingEnrollments > 0) {
                    enrollVerificationProfile(endPoint, accessKey, verificationProfileId, voice, callback)
                } else {
                    console.log(JSON.stringify(body));
                    callback({error: "Impossible d'enrôler"})
                }
            }
        } else {
            console.log('Error enrollment = ' + resp.statusCode);
            if (err) {
                throw(err);
            } else {
                callback({error: JSON.stringify(resp)})
            }
        }
    })
}

function verification(endPoint, accessKey, verificationId, voice, callback) {
//    var path = '/spid/v1.0/identify?identificationProfileIds=' + identificationId + '&shortAudio=true';
    var path = '/spid/v1.0/verify?verificationProfileId=' + verificationId;

    let requestParamsPost = {
        headers: {
            'Ocp-Apim-Subscription-Key': accessKey,
        },
        body: voice
    };
    // Call verification service
    request.post(endPoint + path, requestParamsPost, (err, resp, body) => {
        if (resp.statusCode === 200) {
            callback(body)
        } else {
            if (err) {
                let strErr = JSON.stringify(err)
                console.log('Error identification = ' + strErr);
                callback({error: strErr});
            } else {
                callback({error: resp.statusCode});
            }
        }
    })
}



