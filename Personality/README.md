# Personality Insight 

Personality Insight ou révélateur de personnalité est un service basé sur le Natural Language Processing (NLP). Il permet de dégager d'un texte des caractéristiques sur la personne l'ayant écrite. 

## Introduction

Pour pouvoir analyser la personnalité d'un individu, il est nécessaire de récupérer suffisamment de contenu écrit. Plusieurs entrées peuvent être envisagé pour cela :

* Lettre de motivation ou Liste de lettre de motivation (:star: :star: :star: :star: :star:)
* compte Twitter (:star: :star: :star: :star: :star:)
* Email rédigé par le candidat pour PE (:star: :star: :star: :star:)
* Speetch To Text d'un entretient avec conseiller PE (:star: :star: :star:) :arrow_right: mais dommage car perte d'information (tonalité de la voix, timbre etc). ([Startup analyse vocale](https://www.coupdefeel.com/))
* le compte rendue / fiche d'un candidat écrit par un conseiller PE (:star:) :arrow_right: Analyse de la personne qui a écrit ou de la personne décrite ? 

___

### Profils

Le service Personality Insights est basé sur la psychologie de langage combinée aux algorithmes d'analyse de données. Le service analyse le contenu que vous envoyez et renvoie un profil de personnalité pour l'auteur de l'entrée. Le service déduit des caractéristiques de personnalité en fonction de trois modèles :

* Les caractéristiques de personnalité Big Five constituent le modèle le plus utilisé pour décrire de façon générale la manière dont une personne se comporte avec les autres. Le modèle inclut cinq dimensions primaires :
  * L'[amabilité](https://console.bluemix.net/docs/services/personality-insights/agreeableness.html#agreeableness) est la tendance d'une personne à être bienveillante et à coopérer avec les autres.
  * Le [tempérament consciencieux](https://console.bluemix.net/docs/services/personality-insights/conscientiousness.html#conscientiousness) est la tendance d'une personne à agir de façon organisée ou réfléchie.
  * L'[extraversion](https://console.bluemix.net/docs/services/personality-insights/extraversion.html#extraversion) est la tendance d'une personne à rechercher une stimulation auprès des autres.
  * La [portée émotionnelle](https://console.bluemix.net/docs/services/personality-insights/emotional-range.html#emotionalRange) (ou neuroticisme ou réactions naturelles) est la mesure dans laquelle les émotions d'une personne dépendent de l'environnement de celle-ci.
  * L'[ouverture](https://console.bluemix.net/docs/services/personality-insights/openness.html#openness) est la mesure dans laquelle une personne est prête à expérimenter diverses activités.
* Needs : Les caractéristiques de type [Besoins](https://console.bluemix.net/docs/services/personality-insights/needs.html) décrivent quels sont les aspects d'un produit qui trouveront un écho chez une personne. 
* Value :Les caractéristiques de type [Valeurs](https://console.bluemix.net/docs/services/personality-insights/values.html) décrivent les facteurs qui motivent la prise de décision d'une personne.
	

Ces modèles sont élaborés de manière empirique. Par exemple Le modèle des [Big Five](https://fr.wikipedia.org/wiki/Mod%C3%A8le_des_Big_Five_psychologie) constitue plus un repère pour la description et l'étude d'une personnalité plus qu'une théorie.

## API existantes

Plusieurs entreprise expose une API permettant l'analyse de personnalité à partir d'un texte donné.

### I IBM Watson

:link: [Service de Watson](https://www.ibm.com/watson/services/personality-insights/)

:link: [Documentation](https://console.bluemix.net/docs/services/personality-insights/getting-started.html#tutoriel-d-initiation)

:link: [API reference](https://www.ibm.com/watson/developercloud/personality-insights/api/v3/)

#### Inputs
Le service prend en entrée un JSON contenant le texte à étudier.
Les langages supportés sont : l'arabe, l'anglais, le japonais le coréen et l'espagnol. 

Afin de rendre le service précis, il est nécessaire de lui fournir un texte contenant au moins 600 mots. (une lettre de motivation, ~ 200 mots ! Mais possibilité d'en mettre plusieurs)

#### Output

Le service renvoie un objet `Profile` qui pour chaque facette renvoie un score relatif à l'ensemble de la population. Personlity Insight de WAtson renvoie un profil composé du Big 5 de Needs et Value

### Liste d'API

La liste suivante est une liste d'API en relation avec l'analyse de sentiment. 

* [QEmotion](https://www.qemotion.com/) 
* [TheySay](http://www.theysay.io) Analyse de sentiment et d'émotion [EN]



## POC Besoins et cas d'usage 

Problématique : 
Comment aider les conseillers pôle emploi dans leurs travail ? 

1. Dégager, pour un poste donné, les lettres de motivations / Profils les plus pertinentes ? Celles qui nécessite encore du travail ?

2. Crawller la personnalité internet d'un demandeur d'emploi pour l'aider à trouver des offres qui lui correspondent.

### 1. Read My Letter 
	
Vue offre : 
* Une offre est postulé par N candidats. Tous ces candidats ont donc rédigé pour l'occasion une lettre de motivation (et en ont écrit d'autre) 
* Une offre d'emploi exige à chaque fois des critères et une personnalité bien précise. Une fois l'ouverture d'esprit sera plus valorisé que l'altruisme d'une personne par exemple. 
* à l'aide de l'analyse de personnalité, le conseiller pole emploi peut prioritairement présenter l'offre de travail à la personne qui correspond le mieux (Liste filtrante sur les scores, ou diagramme en étoile , etc etc)

En analysant alors en amont l'offre de travail, le conseiller PE dégage lui même des critères de personnalité qu'il juge pertinent pour le poste.
Le service analyse ensuite les candidats ayant postulé à l'offre pour révéler les profils qui sont plus en corrélations avec les attentes.
Le conseiller PE peut ensuite engager les procédures pour les personnes qu'il juge ++. 

Il s'agit d'un outil supplémentaire et complémentaire à ceux qui existe déjà. Il ne s'agit pas ici de remplacer pour un candidat un entretient d'embauche ou un rendez vous avec son conseiller mais faire en sorte que le conseiller lui propose les offres qui sont le plus en adéquation avec son métier et personnalité. 

### 2. Find My Job
#### V1
Vue Candidat :
* Un candidat fournit à pole emploi du contenue (lettre de motivation, FB feed etc etc)
* Dégage un score de personnalité et permet alors aux algorithmes de rechercher d'offre de travail à personnaliser leurs résultats. 

#### V2 

Outil d'orientation des candidats, Leurs fournir une indication quant au bien fondé de leurs démarches vers tel ou tel poste. 

* CRUD LdM pour un utilisateur. Associer plusieurs lettres de motivation à un ID et lui permettre d'en ajouter où supprimer
* Une modification de lettre déclenche une mise à jour des scores de personnalité 
	* Tester la pertinence et la différence des scores avec l'ajout/suppression d'une LdM / cohérence du profil 
	* stocker seulement les clés/critères appropriés pour PE
* Créer un référentiel Clés/Métier :arrow_right: moyen d'associer un métier/poste à un profil par la suite.
	* conseiller PE ? 
* Avec une meilleure connaissance du candidat (#LdM) et un savoir poussé des métiers de PE. être capable de suggérer des offres d'emploi aux candidats. 

Cela permet d'aider les candidats dans leurs démarches. diminuer le temps de recherche d'emploi. Libérer du temps pour les conseillers PE. 



### 3. Better Letter

Toujours dans le cadre des lettres de motivations, un conseiller Pole Emploi ne dispose peut être pas de suffisamment de temps pour toutes les lire et les analyser. Il peut être alors intéressant de traiter et filtrer les lettres de motivations des candidats afin de repérer celles qui correspondent le moins à un poste (tant sur la forme que sur le fond). Le conseiller peut alors demander à rencontrer le candidat pour retravailler avec lui sa lettre 


## TODO 

1. Twitter crawller 
	* Client API twiiter
	* Avec un nom d'utilisateur , récupérer tous les tweets
	* mise en forme et envoie à PI


3 Cas => Un Positif Pour Boulanger 
